﻿#region File Description
//-----------------------------------------------------------------------------
// Ship.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using System;
using GameStateManagement;
#endregion

namespace ChaseCameraSample
{
    class Player
    {
        #region Fields

        private const float MinimumAltitude = 350.0f;
        public float MiniAli
        {
            get { return MinimumAltitude; }
        }

        /// <summary>
        /// A reference to the graphics device used to access the viewport for touch input.
        /// </summary>
        private GraphicsDevice graphicsDevice;

        /// <summary>
        /// Location of ship in world space.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Direction ship is facing.
        /// </summary>
        public Vector3 Direction;

        /// <summary>
        /// Ship's up vector.
        /// </summary>
        public Vector3 Up;

        private Vector3 right;
        /// <summary>
        /// Ship's right vector.
        /// </summary>
        public Vector3 Right
        {
            get { return right; }
            set { right = value; }
        }

        /// <summary>
        /// Full speed at which ship can rotate; measured in radians per second.
        /// </summary>
        private const float RotationRate = 1.5f;

        public float RotationRate1
        {
            get { return RotationRate; }
        }
        
        /// <summary>
        /// Mass of ship.
        /// </summary>
        private const float Mass = 1.0f;
        public float Mass1
        {
            get { return Mass; }
        }

        /// <summary>
        /// Maximum force that can be applied along the ship's direction.
        /// </summary>
        private const float ThrustForce = 24000.0f;
        public float ThrustForce1
        {
            get { return ThrustForce; }
        }

        /// <summary>
        /// Velocity scalar to approximate drag.
        /// </summary>
        private const float DragFactor = 0.97f;
        public float DragFactor1
        {
            get { return DragFactor; }
        }

        /// <summary>
        /// Current ship velocity.
        /// </summary>
        public Vector3 Velocity;

        /// <summary>
        /// Ship world transform matrix.
        /// </summary>
        public Matrix World
        {
            get { return world; }
            set { world = value; }
        }
        private Matrix world;

        private float change;
        public float Change
        {
            get { return change; }
            set { change = value; }
        }


        private BoundingBox boundingB;
        public BoundingBox BoundingB
        {
            get { return boundingB; }
            set { boundingB = value; }
        }

        private int pNo;

        #endregion

        #region Initialization

        public Player(GraphicsDevice device, int p)
        {
            graphicsDevice = device;
            pNo = p;
            Reset();

            switch (pNo)
            {
                case 0:
                    Direction = Vector3.Forward;
                    right = Vector3.Right;
                    break;
                case 1:
                    Direction = Vector3.Backward;
                    right = Vector3.Left;
                    break;
                default:
                    break;

            }
            Up = Vector3.Up;
            world = Matrix.Identity;
            world.Forward = Direction;
            world.Up = Up;
            world.Right = Right;
            Velocity = Vector3.Zero;
        }

        /// <summary>
        /// Restore the ship to its original starting state
        /// </summary>
        public void Reset()
        {
            switch (pNo)
            {
                case 0:
                    Position = new Vector3(0, MinimumAltitude, 17000);
                    break;
                case 1:
                    Position = new Vector3(0, MinimumAltitude, -20000);
                    break;
                default:
                    break;

            }
            change = 0.0f;

            //boundingB = new BoundingBox(
        }

        #endregion
        /// <summary>
        /// Applies a simple rotation to the ship and animates position based
        /// on simple linear motion physics.
        /// </summary>
        public void Update(GameTime gameTime)
        {
            
        }

        public void updatePlayer(KeyboardState ks, GamePadState gs1, GameTime gameTime, int player, InputState input)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            GamePadState gs = input.CurrentGamePadStates[player];
            // Determine rotation amount from input
            Vector2 rotationAmount = Vector2.Zero;

            Vector3 d = Vector3.Zero;
            if (player == 0)
                d.X = gs.ThumbSticks.Left.X;
            else
                d.X = -gs.ThumbSticks.Left.X;

            // Scale rotation amount to radians per second
            rotationAmount = rotationAmount * RotationRate1 * elapsed;

            // Correct the X axis steering when the ship is upside down
            if (Up.Y < 0)
                rotationAmount.X = -rotationAmount.X;


            // Create rotation matrix from rotation amount
            Matrix rotationMatrix =
                Matrix.CreateFromAxisAngle(Right, rotationAmount.Y) *
                Matrix.CreateRotationY(rotationAmount.X);

            // Rotate orientation vectors
            Direction = Vector3.TransformNormal(Direction, rotationMatrix);
            Up = Vector3.TransformNormal(Up, rotationMatrix);

            // Re-normalize orientation vectors
            // Without this, the matrix transformations may introduce small rounding
            // errors which add up over time and could destabilize the ship.
            Direction.Normalize();
            Up.Normalize();

            // Re-calculate Right
            Right = Vector3.Cross(Direction, Up);

            // The same instability may cause the 3 orientation vectors may
            // also diverge. Either the Up or Direction vector needs to be
            // re-computed with a cross product to ensure orthagonality
            Up = Vector3.Cross(Right, Direction);


            // Determine thrust amount from input
            float thrustAmount = 0.0f;
            //if (ks.IsKeyDown(Keys.Space))
            //    thrustAmount = 1.0f;

            // Calculate force from thrust amount
            Vector3 force = Direction * thrustAmount * ThrustForce1;


            // Apply acceleration
            Vector3 acceleration = force / Mass1;
            Velocity += acceleration * elapsed;

            
            // Apply psuedo drag
            Velocity *= DragFactor1;

            // Apply velocity
            Position += Velocity * elapsed;
            Position.X += d.X * 200.0f;
            change = d.X * 100.0f;


            // Prevent ship from flying under the ground
            Position.Y = Math.Max(Position.Y, MiniAli);

            
            if (Position.X > 5500)
            {
                Position.X = 5500;
            }
            if (Position.X < -5000)
            {
                Position.X = -5000;
            }
            
            // Reconstruct the ship's world matrix
            //s.World = Matrix.Identity;
            world.Translation = Position;

        }
    }
}
