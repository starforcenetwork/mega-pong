#region File Description
//-----------------------------------------------------------------------------
// GameplayScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using ChaseCameraSample;
using lol1;
#endregion

namespace GameStateManagement
{
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>
    class GameplayScreen : GameScreen
    {
        #region Fields 

        SpriteFont spriteFont;
        ChaseCamera camera, camerupt, miniCam;
        Player p1, p2;
        Ball ball;
        Model shipModel, sheepModel;
        Model groundModel;
        Model ballModel;
        BoundingSphere ballBS;
        Texture2D charScreen, a1, a2;
        Vector2 s1v, s2v;
        int n1, n2, s1, s2;
        float v1, v2;
        bool cameraSpringEnabled = true;
        bool vertical;
        bool playing;
        float i = 2000;
        SoundEffectInstance soundeff;

        AudioEmitter emitter = new AudioEmitter();
        AudioListener listener = new AudioListener();

        // Define the viewports that we wish to render to. We will draw two viewports:
        // - The top half of the screen
        // - The bottom half of the screen
        Viewport playerOneViewport;
        Viewport playerTwoViewport;
        Viewport minimap;
        Viewport score;

        KeyboardState keyboardState;
        GamePadState gamePadState;

        Texture2D blank, safeArea;
        GameTime gt;
        ContentManager content; 
        #endregion 
 
        #region Initialization 
 
        public GameplayScreen(bool vert, AudioEngine ae, SoundBank sb, WaveBank wb, WaveBank mwb) 
        { 
            TransitionOnTime = TimeSpan.FromSeconds(1.5); 
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
            vertical = vert;
            playing = false;
        } 
 
        public override void LoadContent() 
        { 
            if (content == null) 
            content=new ContentManager(ScreenManager.Game.Services,"Content"); 
            // TODO: Load content 
 
            // once the load has finished, we use ResetElapsedTime to tell the game's 
            // timing mechanism that we have just finished a very long frame, 
            // and that it should not try to catch up. 
            ScreenManager.Game.ResetElapsedTime();


            // setup the players
            p1 = new Player(ScreenManager.GraphicsDevice, 0);
            p2 = new Player(ScreenManager.GraphicsDevice, 1);
            ball = new Ball(ScreenManager.GraphicsDevice);

            // setup the camera 
            camera = new ChaseCamera();
            camera.DesiredPositionOffset = new Vector3(0.0f, 1300.0f, 4000.0f);
            camera.LookAtOffset = new Vector3(0.0f, 150.0f, 0.0f);
            camera.NearPlaneDistance = 10.0f;
            camera.FarPlaneDistance = 100000.0f;
            camera.AspectRatio =
            (float)ScreenManager.GraphicsDevice.Viewport.Width /
           ScreenManager.GraphicsDevice.Viewport.Height;

            // setup the camera 
            camerupt = new ChaseCamera();
            camerupt.DesiredPositionOffset = new Vector3(0.0f, 1300.0f, 4000.0f);
            camerupt.LookAtOffset = new Vector3(0.0f, 150.0f, 0.0f);
            camerupt.NearPlaneDistance = 10.0f;
            camerupt.FarPlaneDistance = 100000.0f;
            camerupt.AspectRatio =
            (float)ScreenManager.GraphicsDevice.Viewport.Width /
           ScreenManager.GraphicsDevice.Viewport.Height;

            // setup the camera 
            miniCam = new ChaseCamera();
            miniCam.DesiredPositionOffset = new Vector3(0.0f, 20000, 1000.0f);
            miniCam.LookAtOffset = new Vector3(0.0f, 150.0f, 0.0f);
            miniCam.NearPlaneDistance = 10.0f;
            miniCam.FarPlaneDistance = 100000.0f;
            miniCam.AspectRatio =
            (float)ScreenManager.GraphicsDevice.Viewport.Width /
           ScreenManager.GraphicsDevice.Viewport.Height;

            // Perform an inital reset on the camera so that it starts at the 
            // resting position. If we don't do this, the camera will start 
            // at the origin and race across the world to get behind the 
            // chased object. This is performed here because the aspect ratio 
            // is needed by Reset. 
            //UpdateCameraChaseTarget();
            //camera.Reset();
            //camerupt.Reset();

            // load content 
            spriteFont = content.Load<SpriteFont>("gamefont");
<<<<<<< HEAD
            shipModel = content.Load<Model>("LAZORMAN");
            sheepModel = content.Load<Model>("Metalman");
=======
            shipModel = content.Load<Model>("Metalman");
>>>>>>> 93e37a1890ce2703ef07f61739050dbfcaf11d6c
            groundModel = content.Load<Model>("Stadium");
            safeArea = content.Load<Texture2D>("SafeArea720");
            ballModel = content.Load<Model>("Ball");
            ballBS = new BoundingSphere(ball.Position, 30);
            charScreen = content.Load<Texture2D>("Choose");
            a1 = content.Load<Texture2D>("P1");
            a2 = content.Load<Texture2D>("P2");
            n1 = 0;
            n2 = 0;
            s1 = 0;
            s2 = 0;
            v1 = 0;
            v2 = 0;
            ScreenManager.PlayCue.Play();

            ScreenManager.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            blank = new Texture2D(ScreenManager.GraphicsDevice, 1, 1);
            blank.SetData(new[] { Color.White });

            // Create the viewports
            if (vertical)
            {
                playerOneViewport = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = 0,
                    Y = 0,
                    Width = ScreenManager.GraphicsDevice.Viewport.Width / 2,
                    Height = ScreenManager.GraphicsDevice.Viewport.Height / 2,
                };
                playerTwoViewport = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = 0,
                    Y = ScreenManager.GraphicsDevice.Viewport.Height / 2,
                    Width = ScreenManager.GraphicsDevice.Viewport.Width / 2,
                    Height = ScreenManager.GraphicsDevice.Viewport.Height / 2,
                };
                score = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = ScreenManager.GraphicsDevice.Viewport.Width / 2,
                    Y = ScreenManager.GraphicsDevice.Viewport.Height / 2,
                    Width = ScreenManager.GraphicsDevice.Viewport.Width / 2,
                    Height = ScreenManager.GraphicsDevice.Viewport.Height / 2,
                };

                s1v = new Vector2(800, 450);
                s2v = new Vector2(800, 550);
            }
            else
            {
                playerOneViewport = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = 0,
                    Y = ScreenManager.GraphicsDevice.Viewport.Height / 2,
                    Width = ScreenManager.GraphicsDevice.Viewport.Width / 2,
                    Height = ScreenManager.GraphicsDevice.Viewport.Height / 2,
                };
                playerTwoViewport = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = ScreenManager.GraphicsDevice.Viewport.Width / 2,
                    Y = ScreenManager.GraphicsDevice.Viewport.Height / 2,
                    Width = ScreenManager.GraphicsDevice.Viewport.Width / 2,
                    Height = ScreenManager.GraphicsDevice.Viewport.Height / 2,
                };
                score = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = 0,
                    Y = ScreenManager.GraphicsDevice.Viewport.Height / 2,
                    Width = ScreenManager.GraphicsDevice.Viewport.Width / 2,
                    Height = ScreenManager.GraphicsDevice.Viewport.Height / 2,
                };
                s1v = new Vector2(200, 150);
                s2v = new Vector2(200, 250);
            }

            minimap = new Viewport
            {
                MinDepth = 0,
                MaxDepth = 1,
                X = ScreenManager.GraphicsDevice.Viewport.Width / 2,
                Y = 0,
                Width = ScreenManager.GraphicsDevice.Viewport.Width / 2,
                Height = ScreenManager.GraphicsDevice.Viewport.Height / 2,
            };

            

        } 
 
        public override void UnloadContent() 
        { 
            content.Unload(); 
        }       
        #endregion 
 
        #region Update and Draw 
 
        /// <summary> 
        /// Updates the state of the game. This method checks the 
        /// GameScreen.IsActive property, so the game will stop updating 
        /// when the pause menu is active, or if you tab away to a different application. 
        /// </summary> 
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, 
        bool coveredByOtherScreen)
        {
            /*
            emitter.Position = ball.Position;
            soundeff.Apply3D(listener, emitter);
             * */
            ScreenManager.AudioEngine.Update();
            base.Update(gameTime, otherScreenHasFocus, false); 
            if (IsActive) 
            { 
                // TODO: this game isn't very fun! You could probably improve 
                // it by inserting something more interesting in this space :-) 
            }

            if (playing)
            {
                // Update the camera to chase the new target 
                UpdateCameraChaseTarget();
                collideBall();
                // The chase camera's update behavior is the springs, but we
                // can use the Reset method to have a locked, spring-less 
                // camera 
                if (cameraSpringEnabled)
                {
                    camera.Update(gameTime);
                    camerupt.Update(gameTime);
                    miniCam.Update(gameTime);
                }
                else
                {
                    camera.Reset();
                    camerupt.Reset();
                    miniCam.Reset();
                }

                float time = (float)gameTime.TotalGameTime.TotalSeconds;
                /*
                playerTwoView = Matrix.CreateLookAt(
                    new Vector3((float)Math.Cos(time), 1f, (float)Math.Sin(time)) * 800f,
                    Vector3.Zero,
                    Vector3.Up);
                */
                ball.Update(gameTime);

                if (ball.Position.X > 7000)
                {
                    ScreenManager.SoundBank.GetCue("bump").Play();
                }
                if (ball.Position.X < -6500)
                {
                    ScreenManager.SoundBank.GetCue("bump").Play();
                }
            }
            gt = gameTime;
            
            if (v1 > 0)
            {
                v1 -= (float)gt.ElapsedGameTime.TotalSeconds;
                GamePad.SetVibration(PlayerIndex.One, 1.0f, 1.0f);
            }
            else
            {
                GamePad.SetVibration(PlayerIndex.One, 0.0f, 0.0f);
            }
            if (v2 > 0)
            {
                v2 -= (float)gt.ElapsedGameTime.TotalSeconds;
                GamePad.SetVibration(PlayerIndex.Two, 1.0f, 1.0f);
            }
            else
            {
                GamePad.SetVibration(PlayerIndex.Two, 0.0f, 0.0f);
            }
            
        } 
 
        /// <summary> 
        /// Lets the game respond to player input. Unlike the Update method, 
        /// this will only be called when the gameplay screen is active. 
        /// </summary> 
        public override void HandleInput(InputState input) 
        { 
            if (input == null) 
            throw new ArgumentNullException("input"); 
 
            // Look up inputs for the active player profile. 
            int playerIndex = (int)ControllingPlayer.Value;


            keyboardState = input.CurrentKeyboardStates[playerIndex];
            gamePadState = input.CurrentGamePadStates[playerIndex];
            GamePadState pickHero1 = input.CurrentGamePadStates[0];
            GamePadState pickHero2 = input.CurrentGamePadStates[1];
            // The game pauses either if the user presses the pause button, or if they unplug the 
            // active gamepad. This requires us to keep track of whether a gamepad was ever plugged 
            // in, because we don't want to pause on PC if they are playing with a keyboard and 
             
            // have no gamepad at all! 
            bool gamePadDisconnected = !gamePadState.IsConnected && 
            input.GamePadWasConnected[playerIndex]; 
            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected) 
            { 
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer); 
                return; 
            } 
            // process the input 

            // Pressing the A button or key toggles the spring behavior 
            // on and off 
            PlayerIndex dummy;
            if (input.IsNewKeyPress(Keys.Z, ControllingPlayer, out dummy) ||
            input.IsNewButtonPress(Buttons.A, ControllingPlayer,
            out dummy))
            {
                cameraSpringEnabled = !cameraSpringEnabled;
            }

            // Reset the ship on R key or right thumb stick clicked 
            if (input.IsNewKeyPress(Keys.R, ControllingPlayer, out dummy) ||
            input.IsNewButtonPress(Buttons.RightStick, ControllingPlayer,
            out dummy))
            {
                //ship.Reset();
                //sheep.Reset();
                //camera.Reset();
                //camerupt.Reset();
            }

            if (keyboardState.IsKeyDown(Keys.A))
            {
                i+=1000;
            }

            //updateShip(keyboardState, gamePadState, ship, gt, 0, input);
            //updateShip(keyboardState, gamePadState, sheep, gt, 1, input);

            if (playing)
            {
                p1.updatePlayer(keyboardState, gamePadState, gt, 0, input);
                p2.updatePlayer(keyboardState, gamePadState, gt, 1, input);
            }
            else
            {
                if (pickHero1.ThumbSticks.Left.X < 0)
                    n1 = 0;
                else if (pickHero1.ThumbSticks.Left.X > 0)
                    n1 = 1;

                if (pickHero2.ThumbSticks.Left.X < 0)
                    n2 = 0;
                else if (pickHero2.ThumbSticks.Left.X > 0)
                    n2 = 1;

                playing = gamePadState.Buttons.A == ButtonState.Pressed;
            }
        } 
 //
        public override void Draw(GameTime gameTime) 
        { 
            // This game has a blue background. Why? Because! 
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,Color.Maroon, 0, 0);

            

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            ScreenManager.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;

            SpriteBatch sb = ScreenManager.SpriteBatch;
            // Draw our scene with all of our viewports and their respective view/projection matrices.
            if (playing)
            {
                DrawScene(gameTime, playerOneViewport, camera.View, camera.Projection, 1);
                DrawScene(gameTime, playerTwoViewport, camerupt.View, camerupt.Projection, 2);
                DrawScene(gameTime, minimap, miniCam.View, miniCam.Projection, 3);
                
                // Now we'll draw the viewport edges on top so we can visualize the viewports more easily.
                DrawViewportEdges(playerOneViewport);
                DrawViewportEdges(playerTwoViewport);
                DrawViewportEdges(minimap);
                DrawViewportEdges(score);
                
                sb.Begin();
                sb.DrawString(spriteFont, ("Player1: " + s1), s1v, Color.Blue);
                sb.DrawString(spriteFont, ("Player2: " + s2), s2v, Color.Blue);
                sb.DrawString(spriteFont, gt.TotalGameTime.TotalSeconds.ToString(), new Vector2(800, 600), Color.Blue);
                sb.DrawString(spriteFont, v1.ToString(), new Vector2(800, 650), Color.Blue);
                sb.DrawString(spriteFont, p1.Change.ToString(), new Vector2(800, 700), Color.Blue);
                sb.End();
            }
            else
            {
                sb.Begin();
                sb.Draw(charScreen, Vector2.Zero, Color.White);
                sb.Draw(a1, new Vector2(100 + (900 * n1), 400), Color.Blue);
                sb.Draw(a2, new Vector2(100 + (900 * n2), 550), Color.Red);
                sb.End();
            }
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();
            spriteBatch.DrawString(ScreenManager.Font, " ",
                new Vector2(100, 100), Color.DarkRed);

#if DEBUG
            //spriteBatch.Draw(safeArea, Vector2.Zero, Color.White);
#endif
            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black. 
            if (TransitionPosition > 0)
                ScreenManager.FadeBackBufferToBlack(1 - TransitionAlpha);
        }
        #endregion 

        private void DrawScene(GameTime gameTime, Viewport viewport, Matrix view, Matrix projection, int player)
        {
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            // Set our viewport. We store the old viewport so we can restore it when we're done in case
            // we want to render to the full viewport at some point.
            Viewport oldViewport = ScreenManager.GraphicsDevice.Viewport;
            ScreenManager.GraphicsDevice.Viewport = viewport;

            // Here we'd want to draw our entire scene. For this sample, that's just the tank.
            //tank.Draw(Matrix.Identity, view, projection);
            DrawModel(groundModel, Matrix.Identity, player);

            
            if (player == 1)
            {
                DrawModel((n2 == 1)?shipModel : sheepModel, p2.World, player);
                DrawModel(ballModel, ball.World, player);
                DrawModel((n1 == 1) ? shipModel : sheepModel, p1.World, player);
            }
            else
            {
                DrawModel((n1 == 1) ? shipModel : sheepModel, p1.World, player);
                DrawModel(ballModel, ball.World, player);
                DrawModel((n2 == 1) ? shipModel : sheepModel, p2.World, player);
            }
            // Now that we're done, set our old viewport back on the device
            ScreenManager.GraphicsDevice.Viewport = oldViewport;
        }

        /// <summary>
        /// A helper to draw the edges of a viewport.
        /// </summary>
        private void DrawViewportEdges(Viewport viewport)
        {
            const int edgeWidth = 2;

            // We now compute four rectangles that make up our edges
            Rectangle topEdge = new Rectangle(
                viewport.X - edgeWidth / 2,
                viewport.Y - edgeWidth / 2,
                viewport.Width + edgeWidth,
                edgeWidth);
            Rectangle bottomEdge = new Rectangle(
                viewport.X - edgeWidth / 2,
                viewport.Y + viewport.Height - edgeWidth / 2,
                viewport.Width + edgeWidth,
                edgeWidth);
            Rectangle leftEdge = new Rectangle(
                viewport.X - edgeWidth / 2,
                viewport.Y - edgeWidth / 2,
                edgeWidth,
                viewport.Height + edgeWidth);
            Rectangle rightEdge = new Rectangle(
                viewport.X + viewport.Width - edgeWidth / 2,
                viewport.Y - edgeWidth / 2,
                edgeWidth,
                viewport.Height + edgeWidth);

            // We just use SpriteBatch to draw the four rectangles
            ScreenManager.SpriteBatch.Begin();
            ScreenManager.SpriteBatch.Draw(blank, topEdge, Color.Black);
            ScreenManager.SpriteBatch.Draw(blank, bottomEdge, Color.Black);
            ScreenManager.SpriteBatch.Draw(blank, leftEdge, Color.Black);
            ScreenManager.SpriteBatch.Draw(blank, rightEdge, Color.Black);
            ScreenManager.SpriteBatch.End();
        }

        // New functions to be added: 
        private void UpdateCameraChaseTarget() 
        { 
            Vector3 offset = new Vector3(-500, 0, 1000);
            Vector3 yOffSet = new Vector3(0, 1000, 0);
            camera.ChasePosition = p1.Position - offset + yOffSet;
            camera.ChaseDirection = p1.Direction;
            camera.Up = p1.Up;

            camerupt.ChasePosition = p2.Position + offset + yOffSet;
            camerupt.ChaseDirection = p2.Direction;
            camerupt.Up = p2.Up;

            miniCam.ChasePosition = new Vector3(0, 1000, 0);
            miniCam.ChaseDirection = new Vector3(20, -1, 1);
            miniCam.Up = Vector3.Up;

            
        } 
        private void DrawModel(Model model, Matrix world, int player) 
        { 
            Matrix[] transforms = new Matrix[model.Bones.Count]; 
            model.CopyAbsoluteBoneTransformsTo(transforms); 
            foreach (ModelMesh mesh in model.Meshes) 
            { 
                foreach (BasicEffect effect in mesh.Effects) 
                {
                    effect.EnableDefaultLighting();
                    effect.EmissiveColor = new Vector3(0, 0, 1);

                    effect.FogEnabled = true;
                    effect.FogColor = Color.Maroon.ToVector3(); // For best results, ake this color whatever your background is.
                    effect.FogStart = 1.0f;
                    effect.FogEnd = 75000.0f;

                    effect.EnableDefaultLighting(); 
                    //effect.PreferPerPixelLighting = true; 
                    effect.World = transforms[mesh.ParentBone.Index] * world; 
                    // Use the matrices provided by the chase camera 
                    if (player == 1)
                    {
                        effect.View = camera.View; 
                        effect.Projection = camera.Projection; 
                    }
                    else if (player == 2)
                    {
                        effect.View = camerupt.View;
                        effect.Projection = camerupt.Projection;
                    }
                    else if (player == 3)
                    {

                        effect.View = miniCam.View;
                        effect.Projection = miniCam.Projection;
                    }
                } 
                mesh.Draw(); 
            } 
        } 
 
        private void DrawOverlayText()
        {
            ScreenManager.SpriteBatch.Begin();
            //string text = "Right Trigger or Spacebar = thrust\n" +
            //"Left Thumb Stick or Arrow keys = steer\n" +
            //"A = toggle camera spring (" +
            //(cameraSpringEnabled ? "on" : "off") + ")";

            //string text = ship.Position.X.ToString() + ", " + ship.Position.Y.ToString() + ", " + ship.Position.Z.ToString();
            string text = p1.World.ToString();
            // Draw the string twice to create a drop shadow, first colored 
            // black and offset one pixel to the bottom right, then again in 
            // white at the intended position. This makes text easier to read 
            // over the background. +
            ScreenManager.SpriteBatch.DrawString(spriteFont, text,
            new Vector2(65, 65), Color.Black);
            ScreenManager.SpriteBatch.DrawString(spriteFont, text,
            new Vector2(64, 64), Color.White);

            ScreenManager.SpriteBatch.End();
        }

        public void collideBall()
        {
            float shieldSize = 1200;
            float x1, x2;
            x1 = p1.Position.X - shieldSize;
            x2 = p1.Position.X + shieldSize;

            if (ball.Position.Z > 14500)
            {
                if (ball.Position.X > x1 && ball.Position.X < x2)
                {
                    ScreenManager.SoundBank.GetCue("bump").Play();
                    ball.Velocity.Z *= -1;
                    ball.Velocity.X += p1.Change;
                    v1 = 0.5f;
                }
                else
                {
                    ball.Reset();
                    s2++;
                }
            }
            else
            {

            }

            float x12 = p2.Position.X - shieldSize;
            float x22 = p2.Position.X + shieldSize;

            if (ball.Position.Z < -17500)
            {
                if (ball.Position.X > x12 && ball.Position.X < x22)
                {
                    ScreenManager.SoundBank.GetCue("bump").Play();
                    ball.Velocity.Z *= -1;
                    ball.Velocity.X += p2.Change;
                    v2 = 0.5f;
                }
                else
                {
                    ball.Reset();
                    s1++;
                }
            }
            else
            {

            } 
        }

    }


}
