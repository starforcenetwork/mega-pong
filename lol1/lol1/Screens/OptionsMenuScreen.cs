#region File Description
//-----------------------------------------------------------------------------
// OptionsMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
#endregion

namespace GameStateManagement
{
    /// <summary>
    /// The options screen is brought up over the top of the main menu
    /// screen, and gives the user a chance to configure the game
    /// in various hopefully useful ways.
    /// </summary>
    class OptionsMenuScreen : MenuScreen
    {
        #region Fields

        MenuEntry splitScreenMenuEntry;
        MenuEntry soundMenuEntry;
        MenuEntry musicMenuEntry;
        int sound = 10, music = 10;
        public bool vert;

        public bool Vert
        {
            get { return vert; }
            set { vert = value; }
        }
        

        #endregion

        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        public OptionsMenuScreen()
            : base("Options")
        {
            // Create our menu entries.
            splitScreenMenuEntry = new MenuEntry(string.Empty);
            soundMenuEntry = new MenuEntry(string.Empty);
            musicMenuEntry = new MenuEntry(string.Empty);
            vert = true;

            MenuEntry back = new MenuEntry("Back");

            // Hook up menu event handlers.
            splitScreenMenuEntry.Selected += SplitScreenMenuEntrySelected;
            soundMenuEntry.Selected += SoundMenuEntrySelected;
            musicMenuEntry.Selected += MusicMenuEntrySelected;
            back.Selected += OnCancel;
            
            // Add entries to the menu.
            MenuEntries.Add(splitScreenMenuEntry);
            MenuEntries.Add(soundMenuEntry);
            MenuEntries.Add(musicMenuEntry);
            MenuEntries.Add(back);
            SetMenuEntryText();
        }


        /// <summary>
        /// Fills in the latest values for the options screen menu text.
        /// </summary>
        void SetMenuEntryText()
        {
            splitScreenMenuEntry.Text = "Screen Orientation: " + (vert? "Vertical": "Horizontal");
            soundMenuEntry.Text = "Sound Effect: " + sound;
            musicMenuEntry.Text = "Music: " + music;
        }


        #endregion


        /// <summary>
        /// Event handler for when the Ungulate menu entry is selected.
        /// </summary>
        void SplitScreenMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {

            vert = !vert;
            SetMenuEntryText();
        }
        
        /// <summary>
        /// Event handler for when the Ungulate menu entry is selected.
        /// </summary>
        void SoundMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            if(sound > 0)
            {
                sound -= 1;

            }
            else
            {
                sound = 10;
            }
            ScreenManager.SoundCategory.SetVolume((float)sound / 10);
            SetMenuEntryText();
        }

        /// <summary>
        /// Event handler for when the Ungulate menu entry is selected.
        /// </summary>
        void MusicMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            if (music > 0)
            {
                music -= 1;
            }
            else
            {
                music = 10;
            }
            ScreenManager.MusicCategory.SetVolume((float)music / 10);
            SetMenuEntryText();
        }
    }
}
