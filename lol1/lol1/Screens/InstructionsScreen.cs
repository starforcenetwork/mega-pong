#region File Description
//-----------------------------------------------------------------------------
// OptionsMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace GameStateManagement
{
    /// <summary>
    /// The options screen is brought up over the top of the main menu
    /// screen, and gives the user a chance to configure the game
    /// in various hopefully useful ways.
    /// </summary>
    class InstructionsScreen : MenuScreen
    {
        #region Fields

        public bool vert;

        public bool Vert
        {
            get { return vert; }
            set { vert = value; }
        }

        ContentManager content;
        Texture2D instructionsText;


        #endregion

        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        public InstructionsScreen()
            : base("Instructions")
        {
            MenuEntry back = new MenuEntry("Back");

            back.Selected += OnCancel;
            
        }


        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            instructionsText = content.Load<Texture2D>("Instructions");
            
            base.LoadContent();
        }

        /// <summary>
        /// Fills in the latest values for the options screen menu text.
        /// </summary>
        void SetMenuEntryText()
        {
        }


        #endregion

        #region Handle Input


        #endregion

        #region Draw

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

            spriteBatch.Begin();
            spriteBatch.Draw(instructionsText, Vector2.Zero, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        #endregion
    }
}
