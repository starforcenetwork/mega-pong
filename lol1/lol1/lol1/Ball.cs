﻿#region File Description
//-----------------------------------------------------------------------------
// Ship.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using System;
using GameStateManagement;
#endregion

namespace ChaseCameraSample
{
    class Ball
    {
        #region Fields

        private const float MinimumAltitude = 350.0f;
        public float MiniAli
        {
            get { return MinimumAltitude; }
        }

        /// <summary>
        /// A reference to the graphics device used to access the viewport for touch input.
        /// </summary>
        private GraphicsDevice graphicsDevice;

        /// <summary>
        /// Location of ship in world space.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Direction ship is facing.
        /// </summary>
        public Vector3 Direction;

        /// <summary>
        /// Ship's up vector.
        /// </summary>
        public Vector3 Up;

        private Vector3 right;
        /// <summary>
        /// Ship's right vector.
        /// </summary>
        public Vector3 Right
        {
            get { return right; }
            set { right = value; }
        }
        
        /// <summary>
        /// Mass of ship.
        /// </summary>
        private const float Mass = 1.0f;
        public float Mass1
        {
            get { return Mass; }
        }

        /// <summary>
        /// Current ship velocity.
        /// </summary>
        public Vector3 Velocity;

        /// <summary>
        /// Ship world transform matrix.
        /// </summary>
        public Matrix World
        {
            get { return world; }
            set { world = value; }
        }
        private Matrix world;

        private BoundingSphere boundingS;
        public BoundingSphere BoundingS
        {
            get { return boundingS; }
            set { boundingS = value; }
        }

        #endregion

        #region Initialization

        public Ball(GraphicsDevice device)
        {
            graphicsDevice = device;
            Reset();
        }

        /// <summary>
        /// Restore the ship to its original starting state
        /// </summary>
        public void Reset()
        {
            //World = Matrix.Identity;
            Position = new Vector3(0, 1300, 0);
            Direction = Vector3.Forward;
            Up = Vector3.Up;
            right = Vector3.Right;
            Velocity = Vector3.Zero;
            world = Matrix.Identity;
            world.Forward = Direction;
            world.Up = Up;
            world.Right = Right;
            world.Translation = Position;
            boundingS = new BoundingSphere(Position, 30);
            Velocity = new Vector3(150, 0, 150);

        }

        #endregion

        bool TouchLeft()
        {
            MouseState mouseState = Mouse.GetState();
            return mouseState.LeftButton == ButtonState.Pressed &&
                mouseState.X <= graphicsDevice.Viewport.Width / 3;
        }

        bool TouchRight()
        {
            MouseState mouseState = Mouse.GetState();
            return mouseState.LeftButton == ButtonState.Pressed &&
                mouseState.X >= 2 * graphicsDevice.Viewport.Width / 3;
        }

        bool TouchDown()
        {
            MouseState mouseState = Mouse.GetState();
            return mouseState.LeftButton == ButtonState.Pressed &&
                mouseState.Y <= graphicsDevice.Viewport.Height / 3;
        }

        bool TouchUp()
        {
            MouseState mouseState = Mouse.GetState();
            return mouseState.LeftButton == ButtonState.Pressed &&
                mouseState.Y >= 2 * graphicsDevice.Viewport.Height / 3;
        }

        /// <summary>
        /// Applies a simple rotation to the ship and animates position based
        /// on simple linear motion physics.
        /// </summary>
        public void Update(GameTime gameTime)
        {
            if (Position.X > 7000)
            {
                Velocity.X *= -1;
            }
            if (Position.X < -6500)
            {
                Velocity.X *= -1;
            }
            if (Position.Z > 18000)
            {
                Velocity.Z *= -1;
            }
            if (Position.Z < -21000)
            {
                Velocity.Z *= -1;
            }
            Position += Velocity;

            Matrix gg = Matrix.Identity;
            gg.Forward = Direction;
            gg.Up = Up;
            gg.Right = Right;
            gg.Translation = Position;
            boundingS.Transform(gg);
            World = gg;
        }

    }
}
